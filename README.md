# ArduUnoRev3API

ArduUnoRev3API is a C++ library that serves an an API for the Arduino Uno Rev. 3.
and compatible USB development boards.
The GitLab group 
https://gitlab.com/RobotsByDerAndere/ 
contains the Project ArduUnoRev3API 
(https://gitlab.com/RobotsByDerAndere/ArduUnoRev3API which contains:
the source folder src with the library folder ArdUnoRev3API1 containing the 
header file ArdUnoRev3API1.h and the implementation source file 
ArdUnoRev3API1.cpp. follow the instructions in the section "Installation" for
installation of this C++ library and inclusion in your programs

# Installation

If using an integrated development environment (IDE), copy the library folder 
ArdUnoRev3API1 and its contents into the directory where your IDE is looking 
for custom libraries. Import the library ArdUnoRev3API1 into the C++ project.
In the file where you want to include the library, add the following line of 
of code before instantiating an instantiation of the class ArduinoUnoRev3Class1
and calling any of its member functions:
```
#include "ArdUnoRev3API1.h"
```

# Copyright notice

Copyright (c) 2018 DerAndere

This software, excluding third-party software components, is licensed under 
the terms of the MIT License (https://opensource.org/licenses/MIT), 
see ./LICENSE. Contents of the directories ./documentation/ 
and ./src/[Project name]/resources is dual-licensed under the terms of the MIT 
License or the Creative Commons Attribution 4.0 license (CC BY 4.0): 
https://creativecommons.org/licenses/by/4.0/legalcode, see 
./documentation/LICENSE. For all third-party components incorporated into 
this Software, those components are licensed under the original license 
provided by the owner of the applicable component (see ./LICENSE). Source 
code of this software is linked with third-party libraries that are 
licensed under the original license provided by the owner of the applicable 
library (see ./LICENSE) and comments in the 
source code). If applicable, third-party components are kept in separate 
child directories ./src/[Project name]/dep/[component name]. Please see the 
file ./src/[Project name]/dep/[component name]/LICENSE file (and the file
./src/[Project name]/dep/[component name}/NOTICE file), if provided, in each 
third-party folder, or otherwise see comments in the source code.

// SPDX-License-Identifier: MIT
