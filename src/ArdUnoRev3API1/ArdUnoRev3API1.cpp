/**
 * @title: ArdUnoRev3API1.cpp
 * @version: 2.0
 * @author: DerAndere
 * @created: 2018
 * SPDX-License-identifier: MIT
 * Copyright � DerAndere 2018
 * @license: MIT
 * @info: http://www.it-by-derandere.blogspot.com
 * @language: C++
 * @about: A custom C++ library providing the class ArduinoUnoRev3Class1
 */
#include "Arduino.h"
#include "ArdUnoRev3API1.h"
/**
 * easy to use hardware abstraction layer for Arduino-compatible development
 * boards. Provides Serial.begin(), setup(), loop() and Serial.begin,
 * analogRead(), Serial.println(), etc.
 */

namespace unoRev3API1 {

/**
 * Definition of the class members. Implicitly defined default-constructor:
 * Does not need explicit definition. The following lines contain the
 * definition / initialization of static member variables (here: using unified
 * brace initialization. Because vectors, accept a braced-init-list as an
 * initialization parameter, brace initialization of a vector calls the
 * initializer-list constructor. The result is the same as copy-initialization
 * of the vector using = {...}, but possibly more performant. If the number of
 * initializer clauses is less than the number of members or initializer list
 * is completely empty, the remaining members are value-initialized
 * For std::vector of the standard template library (STL) or C++ standard
 * library, {} initializes an empty
 * vector of size 0 (value initialization by calling the default constructor).
 * For built-in POD variable types, empty braces initializes with value 0
 * (value initialzation by calling the default constructor leads to
 * zero-initialization). In contrast = {0} is a braced-init-list with one
 * element of value 0 and compatible with C. Because all elements of the
 * C-style arrays holding pin numbers are initialized with the value 127, all
 * elements with value 127 can be treated as unused.
 */
ArduinoUnoRev3Class1::ArduinoUnoRev3Class1 () : m_digitalInputPins { 127 }, m_digitalOutputPins { 127 }, m_analogInputPins { 127 }, m_hardwarePWMpins { 127 }, m_emaVals { 0 }, m_emaStateVars { 0 } {
}

/**
 * Attach pins and set pinmode
 * @param: int8_t pin
 * @param: int8_t pinIndex
 * @param: bool isDigital
 * @param: bool isINPUT
 * @returns: void
 * @fn: Attach pins and set pinMode()
 */
void ArduinoUnoRev3Class1::attachIO(int8_t pin, int8_t pinIndex, bool isDigital,
bool isINPUT) {
	if (isDigital == true && isINPUT == true) {
		pinMode(pin, INPUT);
		delay(20);
		m_digitalInputPins[pinIndex] = pin;
	}

	else if (isDigital == true && isINPUT == false) {
		pinMode(pin, OUTPUT);
		digitalWrite(pin, LOW);
		delay(20);
		m_digitalOutputPins[pinIndex] = pin;
	}

	else if (isDigital == false && isINPUT == true && pin < 6 && pinIndex < 6) {
		pinMode(pin, INPUT);
		delay(20);
		m_analogInputPins[pinIndex] = pin;
	}

	else if (isDigital == false && isINPUT == true
			&& (pin >= 6 || pinIndex >= 6)) {
		Serial.println("pin and pinIndex must be int8_t < 6");
	}

	else if (isDigital == false && isINPUT == false && pinIndex < 6
			&& (pin == 3 || pin == 5 || pin == 6 || pin == 9 || pin == 10)) {
		pinMode(pin, INPUT);
		delay(20);
		m_hardwarePWMpins[pinIndex] = pin;
	}

	else {
		Serial.println(
				"pin must be hardware-PWM-enabled (3, (5), (6), 9 or 10) and pinIndex must be uint8_t < 6");
	}
}

int8_t ArduinoUnoRev3Class1::getIO(int8_t pinIndex, bool isDigital, bool isINPUT) {
	if (isDigital == true && isINPUT == true) {
		return m_digitalOutputPins[pinIndex];
	}

	else if (isDigital == true && isINPUT == false) {
		return m_digitalInputPins[pinIndex];
	}

	else if (isDigital == false && isINPUT == true) {
		return m_analogInputPins[pinIndex];
	}

	else {
		return m_hardwarePWMpins[pinIndex];
	}
}

/**
 * Call the function analogRead() (provided by the Arduino.h standard library)
 * for the pin of the ArduinoController which was assigned to the specified
 * inputIndex.
 * @param: uint8_t inputIndex
 * @returns: int (0-1023).
 * @fn: If the corresponding pin connects to an 8-bit analog-to-digital converter
 * such as pins A0-A5 of the Arduino Uno, and if the microcontroller device runs on
 * 5 V (e.g. an Arduino Uno or an Arduino-compatible development board with the
 * voltage regulator set to 5V and the voltage reference set to internal voltage
 * reference of 5 V), analogRead() returns the electric potential over the common
 * (�GND�) and the specified analog pin between 0V and 5V, mapped to an integer
 * value 0-1023.
 */
int ArduinoUnoRev3Class1::analogReadInput(int8_t inputIndex) {
	return analogRead(m_analogInputPins[inputIndex]);
}

/**
 * Return smoothed voltage on analog pin with specified inputIndex.
 * @reference: This code is inspired by
 * https://forum.arduino.cc/index.php?topic=445844.0
 * @param: int8_t inputIndex
 * @param: uint8_t filterWeight = 2: filterWeight of the integer arythmetics
 * exponential moving average lowpass filter.
 * @returns: int (0-1023).
 * @fn: If the corresponding pin connects to an 8-bit analog-to-digital converter
 * such as pins A0-A5 of the Arduino Uno, and if the microcontroller device runs on
 * 5 V (e.g. an Arduino Uno or an Arduino-compatible development board with the
 * voltage regulator set to 5V and the voltage reference set to internal voltage
 * reference of 5V), the electric potential over the common
 * (�GND�) and the specified analog pin between 0V and 5V is measured, mapped to an
 * integer value 0-1023. Smoothing is achieved with a exponential moving average
 * lowpass filter using efficient integer arithmetics. For the exponential moving
 * average lowpass filter, the state variable of the filter (val) is calculated using
 * the state equation
 * val = alpha * val + (1-alpha) * lastVal
 * where lastVal is a variable used as a buffer to store the previous value.
 * For efficient integer arithmetics, substitute alpha = 2^(-filterWeight)
 * where filterWeight is an unsigned integer and multiply both sides of the
 * filter state equation by (2^filterWeigth).
 * m_emaStateVar = val*(2^filterWeight) is used as the new filter state
 * variable. Casting to an unsigned integer is required because bit shift
 * right is only defined for unsigned integrals.
 * @info: https://forum.arduino.cc/index.php?topic=445844.0
 */
int ArduinoUnoRev3Class1::smoothReadInput(int8_t inputIndex, uint8_t filterWeight = (uint8_t)2) {
	int currentAnalogRead = 0;
 	currentAnalogRead = analogRead(m_analogInputPins[inputIndex]);
	m_emaStateVars[inputIndex] = m_emaStateVars[inputIndex]
			- m_emaVals[inputIndex] + currentAnalogRead;
	if (m_emaStateVars[inputIndex] >= 0) {
 		unsigned int emaValUnsignedTemporary = 0U;
		emaValUnsignedTemporary = (unsigned int) m_emaStateVars[inputIndex] >> filterWeight; //< bit shift right and extend the top bit (to preserve the sign).
		m_emaVals[inputIndex] = (int) emaValUnsignedTemporary;
	}
	else {
 		long emaValLongTemporary = 0L;
		emaValLongTemporary = (25L * (long)currentAnalogRead) / 100L + ((100L - 25L) * (long)m_emaVals[inputIndex]) / 100L;
		m_emaVals[inputIndex] = (int) emaValLongTemporary;
	}
	return m_emaVals[inputIndex];
}

} //< End namespace unoRev3API1
