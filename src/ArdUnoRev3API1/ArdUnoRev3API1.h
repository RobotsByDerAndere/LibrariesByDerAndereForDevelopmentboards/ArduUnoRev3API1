/**
 * @title: ArdUnoRev3API1.h
 * @version: 2.0
 * @author: DerAndere
 * @created: 2018
 * SPDX-License-identifier: MIT
 * Copyright � DerAndere 2018
 * @license: MIT
 * @info: http://www.it-by-derandere.blogspot.com
 * @language: C++
 * @about: A custom C++ library providing the class ArduinoUnoClass1
 */

#ifndef ARDUNOREV3API1_H
#define ARDUNOREV3API1_H

#include "Arduino.h"

namespace unoRev3API1 {

class ArduinoUnoRev3Class1 {

	/**
	 * Declaration of public (callable from outside the class scope) and private
	 * class members.
	 */
public:
	/**
	 * Declaration of the default-constructor. It is a special member of the
	 * class that has no type and the identifier is equal to the class name.
	 */
	ArduinoUnoRev3Class1();

	/**
	 * The following lines declare public member functions.
	 */

 	/**
 	 * Attach pins and set pinmode
 	 * @param: int8_t pin
 	 * @param: int8_t pinIndex
 	 * @param: bool isDigital
 	 * @param: bool isINPUT
 	 * @returns: void
 	 * @fn: Attach pins and set pinMode()
 	 */
	void attachIO(int8_t /* pin */, int8_t /* pinIndex */, bool /* isDigital */,
	bool /* isINPUT */);

 	/**
 	 * Call the function analogRead() (provided by the Arduino.h standard library)
 	 * for the pin of the ArduinoController which was assigned to the specified
 	 * inputIndex.
 	 * @param: uint8_t inputIndex
 	 * @returns: int (0-1023).
 	 * @fn: If the corresponding pin connects to an 8-bit analog-to-digital
 	 * converter such as pins A0-A5 of the Arduino Uno, and if the microcontroller
 * device runs on 5 V (e.g. an Arduino Uno or an Arduino compatible
 * development board with the voltage regulator set to 5V and the voltage
 * reference set to internal voltage reference of 5 V), analogRead() returns
 * the electric potential over the common (�GND�) and the specified analog pin
 * between 0V and 5V, mapped to an integer value 0-1023.
 */
	int analogReadInput(int8_t /* pinIndex */);

 	/**
 	 * Return smoothed voltage on analog pin with specified inputIndex.
 	 * @reference: This code is inspired by
 	 * https://forum.arduino.cc/index.php?topic=445844.0
 	 * @param: int8_t inputIndex
 	 * @param: uint8_t filterWeight = 2: filterWeight of the integer arythmetics
 	 * exponential moving average lowpass filter.
 	 * @returns: int (0-1023). If the corresponding pin connects to an 8-bit
 	 * analog-to-digital
 	 * @fn: exponential moving average lowpass filter using efficient integer
 	 * arithmetics. For more info, see
 	 * https://forum.arduino.cc/index.php?topic=445844.0. For the exponential
 	 * moving average lowpass filter, the state variable of the filter (val) is
 	 * calculated using the state equation val = alpha * val + (1-alpha) * lastVal
 	 * where lastVal is a variable used as a buffer to store the previous value.
 	 * For efficient integer arithmetics, substitute alpha = 2^(-filterWeight)
 	 * where filterWeight is an unsigned integer and multiply both sides of the
 	 * filter state equation by (2^filterWeigth).
 	 * m_emaStateVar = val*(2^filterWeight) is used as the new filter state
 	 * variable. Casting to an unsigned integer is required because bit shift
 	 * right is only defined for unsigned integrals.
 	 */
	int smoothReadInput(int8_t /* pinIndex */, uint8_t /* filterWeight */);
	int8_t getIO(int8_t /* pinIndex */, bool /* isDigital */, bool /* isINPUT */);

private:
	/**
	 * The following lines declare and define (initialize) private member
	 * variables. For convenience, from C++11 onwards, in-class member
	 * initiation is possible, but braced initiation lists in a member
	 * initializer takes precedence. Until C++14, in-class initiation with a
	 * brace-or-equal-initializer violates constraints for an aggregate type.
	 */
	int8_t m_digitalInputPins[32];
	int8_t m_digitalOutputPins[32];
	int8_t m_analogInputPins[25];
	int8_t m_hardwarePWMpins[22];
	int m_emaVals[25];
	int m_emaStateVars[25];

};  //< End class definition with a closing bracket followed by a semicolon

} //< End namespace unoRev3API1


//Do not add code below this line
#endif /* ARDUNOREV3API1_H */
